/*
Реализуйте свой стек. Решения, использующие std::stack, получат 1 балл. Решения, хранящие стек в массиве, получат 1.5 балла. Решения, использующие указатели, получат 2 балла.

Гарантируется, что количество элементов в стеке ни в какой момент времени не превышает 10000.

Обработайте следующие запросы:

push n: добавить число n в конец стека и вывести «ok»;
pop: удалить из стека последний элемент и вывести его значение, либо вывести «error», если стек был пуст;
back: сообщить значение последнего элемента стека, либо вывести «error», если стек пуст;
size: вывести количество элементов в стеке;
clear: опустошить стек и вывести «ok»;
exit: вывести «bye» и завершить работу.
*/

#include<iostream>
#include<string>

struct Node {
    int value;
    Node* previos;
};

struct Stack {
    public:
        int length = 0;
        Node* top = new Node();
        
        bool is_empty() {
            return top -> previos == nullptr;
        }
        void push(int number) {
            ++length;
            Node* New  = new Node();
            New -> value = number;
            New -> previos = top;
            top = New;
        }
        int pop() {
            --length;
            Node* tail = top -> previos;
            int del_element = top -> value;
            delete top;
            top = tail;
            return del_element;
        }
        int back() {
            return top -> value;
        }
        int size() {
            return length;
        }
        void clear() {
            while (length > 0) {
                --length;
                Node* tail = top -> previos;
                delete top;
                top = tail;
            }
        }
        void exit() {
            delete top;
        }
};

int main() {
    Stack stack;
    
    int number;
    std::string command;

    while (true) {
        std::cin >> command;

        if (command == "push") {
            std::cin >> number;
            std::cout << "ok" << '\n';
            stack.push(number);

        } else if (command == "pop") {
            if (stack.is_empty()) std::cout << "error" << '\n';
            else std::cout << stack.pop() << '\n';
        
        } else if (command == "back") {
            if (stack.is_empty()) std::cout << "error" << '\n';
            else std::cout << stack.back() << '\n';
        
        } else if (command == "size") {
            std::cout << stack.size() << '\n';
        
        } else if (command == "clear") {
            std::cout << "ok" << '\n';
            stack.clear();
        
        } else {
            std::cout << "bye" << '\n';
            stack.clear();
            stack.exit();
            return 0;
        }
    }
}

