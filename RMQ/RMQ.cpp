#include <iostream>
#include <vector>
#include <set>
#include<limits>

const std::pair<int, int> nullpair = {0, 0};
const int INF = std::numeric_limits<int>::max();

class Sparse_table {
public:

    explicit Sparse_table (const std::vector<int>& array);

    int get_min (int left, int right) const;

private:
    std::vector<int> number;
    std::vector<int> power_of_two;
    std::vector<std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>>> sparse;

    void build (int size);
    void write_power_of_two(int size);
};

Sparse_table::Sparse_table (const std::vector<int>& array) {

    int n = array.size();
    std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> temp;
    sparse.push_back(temp);
        
    for (size_t i = 0; i < n; ++i) 
        sparse[0].push_back({{array[i], i}, {INF, -1}});

    build(n);
    write_power_of_two(n);
}    

void Sparse_table::build(int size) {
    int two_in_range = 2;
    size_t j;

    for (size_t range = 0; two_in_range < size; ++range) {
            
        std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> temp;
        sparse.push_back(temp);
            
        for (size_t i = 0; i < (size - two_in_range + 1); ++i) {
            sparse[range + 1].push_back({nullpair, nullpair});                

            std::set<std::pair<int, int>> set_for_min_now;

            j = std::min(sparse[range].size() - 1, i + (two_in_range / 2));
            
            set_for_min_now.insert(sparse[range][i].first);
            set_for_min_now.insert(sparse[range][i].second);
            set_for_min_now.insert(sparse[range][j].first);
            set_for_min_now.insert(sparse[range][j].second);
                
            sparse[range + 1][i] = {*(set_for_min_now.begin()), *next(set_for_min_now.begin())};
        }
        two_in_range *= 2;
    }
}

void Sparse_table::write_power_of_two(int size) {

    number.reserve(size);
    power_of_two.reserve(size);

    number[0] = 0;
    power_of_two[0] = 0;

    int now_number = 0, now_digit_two = 1;

    for (size_t i = 1; i < size; ++i) {
            
        if (((i&(i-1)) == 0) && (i != 1)) {
            ++now_number;
            now_digit_two *= 2;
        }

        number[i] = now_number;
        power_of_two[i] = now_digit_two;
    }
}

int Sparse_table::get_min (int left, int right) const {
    int range = right - left;

    std::set<std::pair<int, int>> set_for_min;
        
    set_for_min.insert(sparse[number[range]][left].first);
    set_for_min.insert(sparse[number[range]][left].second);
    set_for_min.insert(sparse[number[range]][right - power_of_two[range] + 1].first);
    set_for_min.insert(sparse[number[range]][right - power_of_two[range] + 1].second);
        
    return (*(next(set_for_min.begin()))).first;
}

int main() {
    int n, m;
    std::cin >> n >> m;

    std::vector<int> array(n);

    for (size_t i = 0; i < n; ++i) {
        std::cin >> array[i];
    }

    Sparse_table table(array);

    int left, right;
    
    for (size_t i = 0; i < m; ++i) {
        std::cin >> left >> right;
        std::cout << table.get_min(left - 1, right - 1) << '\n';
    }
}
 
