/*
Напишите программу, которая для заданного массива A=⟨a1,a2,…,an⟩ находит количество пар (i,j) таких, что i < j и a[i] > a[j].
Обратите внимание на то, что ответ может не влезать в int.
*/

#include <iostream>
#include <vector>
#include <fstream>

long long merge(std::vector<int> &array, int left, int middle, int right) {
    long long count = 0;
    
    std::vector<int> copy_array(right - left + 1);
    
    int i = left, j = middle + 1, k = 0;
 
    while(i <= middle && j <= right) {
        if(array[i] < array[j]) {
            copy_array[k++] = array[i++];
        
        }else {
            copy_array[k++] = array[j++];
            count += middle - i + 1;
        }
    }

    while(i <= middle) { 
        copy_array[k++] = array[i++];
    }
    while(j <= right) {
        copy_array[k++] = array[j++];
    }
    
    for(int i = left; i < right + 1; ++i) {
        array[i] = copy_array[i - left];
    }

    return count;
}

long long count_inversions(std::vector<int> &array, int left, int right) {
    long long count = 0;
    if(left >= right) {
        return 0;
    }
 
    int middle = (left + right) / 2;
    count += count_inversions(array, left, middle);
    count += count_inversions(array, middle + 1, right);
 
    count += merge(array, left, middle, right);
    return count;
}


int main() {
    std::ios_base::sync_with_stdio(false);
	std::cout.tie(0);
	std::cin.tie(0);
    
    freopen("inverse.in", "r", stdin);
    freopen("inverse.out", "w", stdout);

    int n;
    std::cin >> n;
    std::vector<int> array(n);

    for(int i = 0; i < n; ++i) {
        std::cin >> array[i];
    }

    std::cout << count_inversions(array, 0, n - 1);
}

