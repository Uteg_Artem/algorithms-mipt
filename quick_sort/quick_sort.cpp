/*
Отсортируйте данную последовательность, используя алгоритм быстрой сортировки QuickSort.
*/

#include<iostream>
#include<vector>

std::pair<int, int> partition (std::vector<int>& array, int left, int right) {
    
    int pivot_index = rand() % (right - left) + left;

    std::swap(array[pivot_index], array[left]);
    
    int pivot = array[left];


    while (left <= right) {

        while (array[left] < pivot) ++left;
        while (array[right]  > pivot) --right;
        
        if (left <= right) {
            std::swap(array[right], array[left]);
            ++left;
            --right;
        }
    }


    return {right, left};
} 

void quick_sort(std::vector<int>& array, int left, int right) {
    
    if (right > left) {
        std::pair<int, int> pivot = partition(array, left, right); 
        
        int border_left = pivot.first;
        int border_right = pivot.second;

        quick_sort(array, left, border_left); 
        quick_sort(array, border_right, right); 
    } 
} 

int main() {

    std::ios_base::sync_with_stdio(false);
    std::cout.tie(0);
    std::cin.tie(0);

    int n;
    std::cin >> n;
    std::vector<int> array(n);
    
    for (size_t i = 0; i < n; ++i) {
        std::cin >> array[i];
    }
    
    quick_sort(array, 0, n - 1);

    for (size_t i = 0; i < n; i++) {
        std::cout << array[i] << " "; 
    }
}

