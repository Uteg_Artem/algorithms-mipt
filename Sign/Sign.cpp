/*
Реализуйте структуру данных из n элементов a1,a2…an, поддерживающую следующие операции:

присвоить элементу ai значение j;
найти знакочередующуюся сумму на отрезке от l до r включительно (al−al+1+al+2−…±ar). 
*/

#include <iostream>
#include <vector>

class Binary_indexed_tree {

public:

    explicit Binary_indexed_tree (const std::vector<int>& array);
    
    void change (size_t index, int value);
    int get_sum (int left, int right) const;

private:

    std::vector<int> tree;
    
    int get_sum (size_t index) const;

    static int f (int x) {return (x + 1) & x; }
    static int g (int x) {return (x + 1) | x; }
    
};

Binary_indexed_tree::Binary_indexed_tree (const std::vector<int>& array) {
    tree.assign(array.size(), 0);
    for (size_t i = 0; i < array.size(); ++i) {
        for (size_t j = f(i); j < i + 1; ++j) {
            tree[i] += array[j];
        }
    }
}

void Binary_indexed_tree::change (size_t index, int value) {
    if (index % 2) value *= -1;

    for (size_t i = index; i < tree.size(); i = g(i)) {
        tree[i] += value;
    }
}

int Binary_indexed_tree::get_sum (int left, int right) const {    
    int border_right = get_sum(right),
    border_left = (left > 0) ? get_sum(left - 1) : 0,
    sum = border_right - border_left ;

    if (left % 2) 
        return -sum;

    return sum;
}

int Binary_indexed_tree::get_sum (size_t index) const {
    int answer = 0;

    for(int i = index; i >= 0; i = f(i) - 1) {
        answer += tree[i];
    }
    return answer;
}

int main() {
    int n, x;
    
    std::cin >> n;
    std::vector<int> array(n);

    for (size_t i = 0; i < n; ++i) {
        std::cin >> x;

        array[i] = (i % 2) ? -x : x;
    }

    Binary_indexed_tree tree(array);

    int m, command, i, j;
    std::cin >> m;
    
    for (size_t k = 0; k < m; ++k) {
         
        std::cin >> command >> i >> j;

        if (command == 0) 
            tree.change(i - 1, j - tree.get_sum(i - 1, i - 1));
        
        else 
            std::cout << tree.get_sum(i - 1, j - 1) << '\n';
        
    }
}
