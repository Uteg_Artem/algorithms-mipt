/*
Дан массив неотрицательных целых 64-битных чисел. Количество чисел не больше 300000.
Отсортировать массив методом поразрядной сортировки LSD по байтам.
*/

#include<iostream>
#include<vector>

const int BIT_IN_BYTE = 8;
const int VALUE_BYTE = 256;
const int BYTE_LL = 8;

int bytes(long long number, int n) {
    return number >> (BIT_IN_BYTE * n) & (VALUE_BYTE - 1);
}

void lsd_sort(std::vector<long long>& array) {
    std::vector<int> count(VALUE_BYTE);

    for(int number_byte = 0; number_byte < BYTE_LL; number_byte++) {

        for(int i = 0; i < VALUE_BYTE; i++) {
            count[i] = 0;
        }

        for(int i = 0; i < array.size(); i++) {
            count[bytes(array[i], number_byte)]++;
        }

        for(int i = 1; i < VALUE_BYTE; i++) {
            count[i] += count[i - 1];
        }

        std::vector<long long> copy_array(array.size());
        for(int i = array.size() - 1; i > -1; i--) {
            copy_array[--count[bytes(array[i], number_byte)]] = array[i];
        }

        for(int i = 0; i < array.size(); i++) {
            array[i] = copy_array[i];
        }
    }

}


int main() {
    
    std::ios_base::sync_with_stdio(false);
    std::cout.tie(0);
    std::cin.tie(0);

    int n;
    std::cin >> n;
    std::vector<long long> array(n);
    for(int i = 0; i < n; i++) {
        std::cin >> array[i];
    }

    lsd_sort(array);

    for(int i = 0; i < array.size(); i++) {
        std::cout << array[i] << " ";
    }
}

