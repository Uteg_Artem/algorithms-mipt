/*
Даны неотрицательные целые числа N, K и массив целых чисел из диапазона [0,10^9] размера N.
Требуется найти K-ю порядковую статистику, т.е. напечатать число, 
которое бы стояло на позиции с индексом K ∈[ (0,N−1] в отсортированном массиве.

Реализуйте алгоритм QuickSelect (среднее время работы O(N)). 
*/

#include<iostream>
#include<vector>

int partition(std::vector<int>& array, int left, int right) {
    
    int pivot = array[rand() % (right - left + 1) + left], i = left, j = right;
    
    while (i <= j) {
        
        while (i < array.size() && array[i] <= pivot) ++i;
        while (j > 0 && array[j] > pivot) --j;
        
        if (i >= j) break;

        std::swap(array[i], array[j]);
    }

    return j;
}

int find_kth_statistic (std::vector<int>& array, int k) {
    
    int left = 0, right = array.size() - 1, middle;
    
    while (true) {

        middle = partition(array, left, right);
        
        if (middle == k) return array[middle];

        if (k > middle) left = middle + 1;
        else right = middle;
    }
}

int main () {

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);

    int n, k;

    std::cin >> n >> k;
    std::vector<int> array(n);

    for(size_t i = 0; i < n; ++i) {
        std::cin >> array[i];
    }
    
    std::cout << find_kth_statistic(array, k) << '\n';
}

