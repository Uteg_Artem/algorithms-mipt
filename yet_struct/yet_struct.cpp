/*
Нужно отвечать на запросы вида

•+ x – добавить в мультимножество число x.
•? x – посчитать сумму чисел не больших x.

*/

#include <iostream>

const long long SIZE = 1073741823;

struct Node {
    long long b_l = 0, b_r = SIZE; 
    long long sum = 0;
    
    Node* left = nullptr;
    Node* right = nullptr;
};

struct Dynamic_segment_tree {
public:
    Dynamic_segment_tree() { top = new Node; }

    ~Dynamic_segment_tree() { del(top); }

    void add (long long x, long long left, long long right) { add(top, x, left, right); }

    long long get_sum (long long index) const { return get_sum(top, index); }

private:
    Node* top = nullptr;

    void del(Node* tree);
    
    void add (Node* tree, long long x, long long left, long long right);
    
    long long get_sum (Node* tree, long long index) const;

    bool is_empty_right (Node* tree) const { return tree -> right == nullptr; }
    bool is_empty_left (Node* tree) const { return tree -> left == nullptr; }
    long long middle (Node* tree) const { return (tree -> b_l + tree -> b_r) / 2; }

};

void Dynamic_segment_tree::del(Node* tree) {
    if (!is_empty_left(tree)) del(tree -> left);
    if (!is_empty_right(tree)) del(tree -> right);
    delete tree;
}

void Dynamic_segment_tree::add (Node* tree, long long x, long long left, long long right) {
    if ((tree -> b_l == left) && (tree -> b_r == right)) {
        tree -> sum += x;
        
    } else if (middle(tree) < left) {
        
        if (is_empty_right(tree)) {
            tree -> right = new Node;
            tree -> right -> b_l = middle(tree) + 1;
            tree -> right -> b_r = tree -> b_r;
        }

        add(tree -> right, x, left, right);
    
    } else if (middle(tree) >= right) {
            
        if (is_empty_left(tree)) {
            tree -> left = new Node;
            tree -> left -> b_l = tree -> b_l;
            tree -> left -> b_r = middle(tree);
        }
        add(tree -> left, x, left, right);
        
    } else {
        
        if (is_empty_right(tree)) {
            tree -> right = new Node;
            tree -> right -> b_l = middle(tree) + 1;
            tree -> right -> b_r = tree -> b_r;
        }

        add(tree -> right, x, tree -> right -> b_l, right);
        
        if (is_empty_left(tree)) {
            tree -> left = new Node;
            tree -> left -> b_l = tree -> b_l;
            tree -> left -> b_r = middle(tree);
        }

        add(tree -> left, x, left, tree -> left -> b_r);
    }
}  

long long Dynamic_segment_tree::get_sum (Node* tree, long long index) const {

    if ((!is_empty_left(tree)) && (index <= middle(tree))) 
        return tree -> sum + get_sum(tree -> left, index);
        
    if ((!is_empty_right(tree)) && (index > middle(tree)))
        return tree -> sum + get_sum(tree -> right, index);
        
    return tree -> sum;
}

int main() {
    
    std::ios_base::sync_with_stdio(false);
    std::cout.tie(0);
    std::cin.tie(0);    
    
    Dynamic_segment_tree tree;

    char command; 
    long long q;
    long long x;

    std::cin >> q;
    
    for (size_t i = 0; i < q; ++i) {
        
        std::cin >> command >> x;
        
        if (command == '+') tree.add(x, x, SIZE);
        
        else std::cout << tree.get_sum(x) << std::endl;
    }
}

