/*
Гоблины Мглистых гор очень любях ходить к своим шаманам. Так как гоблинов много, к шаманам часто образуются очень длинные очереди. А поскольку много гоблинов в одном месте быстро образуют шумную толку, которая мешает шаманам проводить сложные медицинские манипуляции, последние решили установить некоторые правила касательно порядка в очереди.

Обычные гоблины при посещении шаманов должны вставать в конец очереди. Привилегированные же гоблины, знающие особый пароль, встают ровно в ее середину, причем при нечетной длине очереди они встают сразу за центром.

Так как гоблины также широко известны своим непочтительным отношением ко всяческим правилам и законам, шаманы попросили вас написать программу, которая бы отслеживала порядок гоблинов в очереди.
*/

#include<iostream>
#include<queue>
#include<deque>

struct Goblins_in_queue {
    private:
        std::queue<int> first_half;
        std::deque<int> second_half;
        void find_balance() {
            if (second_half.size() > first_half.size()) {
                int middle = second_half.front();
                second_half.pop_front();
                first_half.push(middle);
            }
        }
    public:
        void push_common(int number) {
            second_half.push_back(number);
            find_balance();
        }
        void push_favored(int number) {
            second_half.push_front(number);
            find_balance();
        }
        int pop_goblin(){
            int next = first_half.front();
            first_half.pop();
            find_balance();
            return next;
        }
};

int main() {
    int number, n;
    char command;
    std::cin >> n;
    
    Goblins_in_queue goblins;

    for(int i = 0; i < n; ++i) {
        std::cin >> command;
        if(command == '-'){
            std::cout << goblins.pop_goblin() << '\n';
        }else { 
            std::cin >> number;
            if(command == '+') goblins.push_common(number);
            else goblins.push_favored(number);
        }
    }
}

